#include <fstream>
#include <iostream>
#include <fcntl.h>
#include <sstream>
#include <io.h>

#include "sandbox.pb.h"
#include <google/protobuf/text_format.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>

int main()
{
  // Verify that the version of the library that we linked against is
  // compatible with the version of the headers we compiled against.
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  auto MyLogHandler = [](google::protobuf::LogLevel level, const char* filename, int line, const std::string& message)
  {
    std::cout << level << " " << message << " from " << filename << "::" << line << std::endl;
  };
  google::protobuf::SetLogHandler(MyLogHandler);

  std::string msg_a;
  std::string msg_b;

  // Build out our hierarchy
  // Note that the set_allocated methods transfer ownership of the memory
  sandbox::object_track_state_data* obj_trk = new sandbox::object_track_state_data;

  sandbox::track_state_data* state = new sandbox::track_state_data;
  sandbox::raw_descriptor_data* raw_descriptor = new sandbox::raw_descriptor_data;
  raw_descriptor->add_values(0);
  state->mutable_raw_descriptor()->AddAllocated(raw_descriptor);
  obj_trk->set_allocated_state(state);
 
  sandbox::bounding_box_data* bounding_box = new sandbox::bounding_box_data;
  sandbox::point_data* pt1 = new sandbox::point_data;
  sandbox::point_data* pt2 = new sandbox::point_data;
  pt1->set_x(25);
  pt1->set_y(25);
  pt2->set_x(75);
  pt2->set_y(75);
  bounding_box->set_allocated_point_1(pt1);
  bounding_box->set_allocated_point_2(pt2);
  obj_trk->set_allocated_bounding_box(bounding_box);

  // Write out binary
  std::ofstream binary_ostream("object_track.pb", std::ios::out | std::ios::trunc | std::ios::binary);
  obj_trk->SerializeToOstream(&binary_ostream);
  binary_ostream.flush();
  binary_ostream.close();
  // Binary String
  obj_trk->SerializeToString(&msg_b);
  std::cout << "Binary represnetation : \n" << msg_b << std::endl;

  std::cout << std::endl;

  //// Write ascii to file and to string
  google::protobuf::TextFormat::PrintToString(*obj_trk, &msg_a);
  std::cout << "ASCII represnetation : \n" << msg_a << std::endl;
  std::ofstream ascii_ostream("object_track.pba", std::ios::out | std::ios::trunc);
  ascii_ostream<<msg_a;
  ascii_ostream.flush();
  ascii_ostream.close();


  sandbox::object_track_state_data object_track_from_binary_file;
  std::ifstream binary_istream("object_track.pb", std::ios::in | std::ios::binary);
  object_track_from_binary_file.ParseFromIstream(&binary_istream);
  std::cout << "Bounding Box From binary file : (" << object_track_from_binary_file.bounding_box().point_1().x() <<","<< object_track_from_binary_file.bounding_box().point_1().y() << ") "
                                               "(" << object_track_from_binary_file.bounding_box().point_2().x() <<","<< object_track_from_binary_file.bounding_box().point_2().y() << ") " << std::endl;

  sandbox::object_track_state_data object_track_from_binary_string;
  object_track_from_binary_string.ParseFromString(msg_b);
  std::cout << "Bounding Box From binary string : (" << object_track_from_binary_string.bounding_box().point_1().x() <<","<< object_track_from_binary_string.bounding_box().point_1().y() << ") "
                                                 "(" << object_track_from_binary_string.bounding_box().point_2().x() <<","<< object_track_from_binary_string.bounding_box().point_2().y() << ") " << std::endl;

  sandbox::object_track_state_data object_track_from_ascii_file;
  std::ifstream input("object_track.pba");
  std::string fmsg((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());
  google::protobuf::TextFormat::ParseFromString(fmsg, &object_track_from_ascii_file);
  std::cout << "Bounding Box From ascii file : (" << object_track_from_ascii_file.bounding_box().point_1().x() <<","<< object_track_from_ascii_file.bounding_box().point_1().y() << ") "
                                              "(" << object_track_from_ascii_file.bounding_box().point_2().x() <<","<< object_track_from_ascii_file.bounding_box().point_2().y() << ") " << std::endl;

  sandbox::object_track_state_data object_track_from_ascii_string;
  google::protobuf::TextFormat::ParseFromString(msg_a, &object_track_from_ascii_string);
  std::cout << "Bounding Box From ascii string : (" << object_track_from_ascii_string.bounding_box().point_1().x() <<","<< object_track_from_ascii_string.bounding_box().point_1().y() << ") "
                                                "(" << object_track_from_ascii_string.bounding_box().point_2().x() <<","<< object_track_from_ascii_string.bounding_box().point_2().y() << ") " << std::endl;


  // Optional:  Delete all global objects allocated by libprotobuf.
  google::protobuf::ShutdownProtobufLibrary();
  return 0;
}

