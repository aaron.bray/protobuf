cmake_minimum_required(VERSION 3.2)

set(CMAKE_INSTALL_PREFIX ${CMAKE_BINARY_DIR}/install CACHE PATH "Install location")

project(protobuf_sandbox)
# Policy to address @foo@ variable expansion
if(POLICY CMP0053)
  cmake_policy(SET CMP0053 NEW)
endif()
include(ExternalProject)

set(CMAKE_CONFIGURATION_TYPES Debug Release RelWithDebInfo CACHE TYPE INTERNAL FORCE )

if(MSVC)  
  if (BUILD_SHARED_LIBS)
    set(CMAKE_CXX_FLAGS_DEBUG "/D_DEBUG /MDd /Zi /Ob2 /Oi /Od /RTC1" CACHE TYPE INTERNAL FORCE)
    set(CMAKE_CXX_FLAGS_RELEASE " /MD " CACHE TYPE INTERNAL FORCE)
  else()
    set(CMAKE_CXX_FLAGS_DEBUG "/D_DEBUG /MTd /Zi /Ob2 /Oi /Od /RTC1" CACHE TYPE INTERNAL FORCE)
    set(CMAKE_CXX_FLAGS_RELEASE " /MT " CACHE TYPE INTERNAL FORCE)
  endif()
endif()
if(MINGW)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wl,--kill-at -std=gnu++0x")#turn on C++11
  set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--kill-at")
endif()

if(APPLE)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -stdlib=libc++")
endif()

if(UNIX)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_RPATH}:\$ORIGIN")
endif()


set(protobuf_VERSION "3.3.3" )
set(protobuf_DIR "${CMAKE_BINARY_DIR}/protobuf/src/protobuf")
set(protobuf_INSTALL "${CMAKE_CURRENT_BINARY_DIR}/protobuf/install")




set (SUPERBUILD ON CACHE BOOL "Initial pull and build of all dependent libraries/executables")
if(SUPERBUILD)

  ExternalProject_Add( protobuf
    PREFIX protobuf
    GIT_REPOSITORY "https://github.com/google/protobuf.git"
    GIT_SHALLOW 1
    GIT_TAG "v3.3.0"
    SOURCE_SUBDIR ./cmake
    INSTALL_DIR "${protobuf_INSTALL}"
    CMAKE_ARGS 
      -Dprotobuf_BUILD_TESTS:BOOL=OFF
      -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
      -DCMAKE_INSTALL_PREFIX:STRING=${CMAKE_INSTALL_PREFIX}
      -DCMAKE_CXX_COMPILER:FILEPATH=${CMAKE_CXX_COMPILER}
      -DCMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
      -DCMAKE_CXX_FLAGS_DEBUG:STRING=${CMAKE_CXX_FLAGS_DEBUG}
      -DCMAKE_CXX_FLAGS_RELEASE:STRING=${CMAKE_CXX_FLAGS_RELEASE}
      -DCMAKE_C_COMPILER:FILEPATH=${CMAKE_C_COMPILER}
      -DCMAKE_C_FLAGS:STRING=${CMAKE_C_FLAGS}
      ${CMAKE_CXX_COMPILER_LAUNCHER_FLAG}
      ${CMAKE_C_COMPILER_LAUNCHER_FLAG}
      -DCMAKE_EXE_LINKER_FLAGS:STRING=${CMAKE_EXE_LINKER_FLAGS}
      -DCMAKE_SHARED_LINKER_FLAGS:STRING=${CMAKE_SHARED_LINKER_FLAGS}
      -DMAKECOMMAND:STRING=${MAKECOMMAND}
      -DADDITIONAL_C_FLAGS:STRING=${ADDITIONAL_C_FLAGS}
      -DADDITIONAL_CXX_FLAGS:STRING=${ADDITIONAL_CXX_FLAGS}
  )
  
  ExternalProject_Add( Sandbox
    DEPENDS protobuf
    PREFIX Sandbox
    DOWNLOAD_COMMAND ""
    DOWNLOAD_DIR ${CMAKE_SOURCE_DIR}
    SOURCE_DIR ${CMAKE_SOURCE_DIR}
    BINARY_DIR ${CMAKE_BINARY_DIR}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    BUILD_AWAYS 1
    LIST_SEPARATOR ::
    CMAKE_ARGS
      -DSUPERBUILD:BOOL=OFF
      -DCMAKE_PREFIX_PATH:STRING=${CMAKE_PREFIX_PATH}
      -DCMAKE_INSTALL_PREFIX:STRING=${CMAKE_INSTALL_PREFIX}
      -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
      -DCMAKE_CXX_COMPILER:FILEPATH=${CMAKE_CXX_COMPILER}
      -DCMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
      -DCMAKE_CXX_FLAGS_DEBUG:STRING=${CMAKE_CXX_FLAGS_DEBUG}
      -DCMAKE_CXX_FLAGS_RELEASE:STRING=${CMAKE_CXX_FLAGS_RELEASE}
      -DCMAKE_C_COMPILER:FILEPATH=${CMAKE_C_COMPILER}
      -DCMAKE_C_FLAGS:STRING=${CMAKE_C_FLAGS}
      ${CMAKE_CXX_COMPILER_LAUNCHER_FLAG}
      ${CMAKE_C_COMPILER_LAUNCHER_FLAG}
      -DCMAKE_EXE_LINKER_FLAGS:STRING=${CMAKE_EXE_LINKER_FLAGS}
      -DCMAKE_SHARED_LINKER_FLAGS:STRING=${CMAKE_SHARED_LINKER_FLAGS}
      -DMAKECOMMAND:STRING=${MAKECOMMAND}
      -DADDITIONAL_C_FLAGS:STRING=${ADDITIONAL_C_FLAGS}
      -DADDITIONAL_CXX_FLAGS:STRING=${ADDITIONAL_CXX_FLAGS}
  )
  return()
else()
  project(sandbox)
  
  set(_protoc "${CMAKE_INSTALL_PREFIX}/bin/protoc")
  message(STATUS "protoc is here : ${_protoc}")
  set(cpp_bindings_DIR ${CMAKE_BINARY_DIR}/pb/bind)
  file(MAKE_DIRECTORY  ${cpp_bindings_DIR})
  set(_PROTO_FILES)
  list(APPEND _PROTO_FILES ${CMAKE_SOURCE_DIR}/proto/sandbox.proto)
  set(_PB_FILES)
  list(APPEND _PB_FILES ${cpp_bindings_DIR}/sandbox.pb.h)
  list(APPEND _PB_FILES ${cpp_bindings_DIR}/sandbox.pb.cc)

  set(_SOURCE cpp/main.cpp)
  list(APPEND _SOURCE ${_PB_FILES})
  configure_file(${CMAKE_SOURCE_DIR}/protoc.cmake.in ${CMAKE_INSTALL_PREFIX}/bin/protoc.cmake @ONLY)
  execute_process(COMMAND ${CMAKE_COMMAND} -P ${CMAKE_INSTALL_PREFIX}/bin/protoc.cmake)
  link_directories(${CMAKE_INSTALL_PREFIX}/lib)
  add_executable(sandbox_driver ${_SOURCE})
  
  target_include_directories(sandbox_driver PRIVATE ${protobuf_DIR}/src)
  #target_include_directories(sandbox_driver PRIVATE ${CMAKE_INSTALL_PREFIX}/include)
  target_include_directories(sandbox_driver PRIVATE ${cpp_bindings_DIR})
  
  
  if(MSVC)
    target_link_libraries(sandbox_driver PRIVATE optimized libprotobuf)
    target_link_libraries(sandbox_driver PRIVATE debug libprotobufd)
  else()
    target_link_libraries(sandbox_driver PRIVATE libprotobuf)
  endif()
  add_custom_command(TARGET sandbox_driver PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -P ${CMAKE_INSTALL_PREFIX}/bin/protoc.cmake)

  install(TARGETS sandbox_driver DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
endif()